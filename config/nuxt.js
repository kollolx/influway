'use strict'

const resolve = require('path').resolve
const webpack = require('webpack')

module.exports = {


  modules: [
    '@nuxtjs/axios',

  ],
  axios: {
    baseURL: 'https://influway.com'
  },

  build: {
    extractCSS: true
  },

  plugins: [

    '~plugins/axios',
    { src: '~plugins/axios', ssr: false },

    '~plugins/iview',
    { src: '~plugins/iview', ssr: false }

  ],

  /*
  ** Headers of the page
  */




  head: {
    title: 'InfluWay | Decentralized social e-commerce platform',
    meta: [
      {
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1,user-scalable=0'
      },
      {
        name: 'theme-color',
        content: '#000000'
      },
      {
        hid: 'description',
        name: 'description',
        content: 'Adonuxt project'
      }
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: 'favicon.png'
      },
      {
        rel: 'stylesheet',
        href: 'css/styles.css'
      }
    ],
    script: [

      { src: 'https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js' },
      {
        src: 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
        integrity: "sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa",
        crossorigin: "anonymous"
      },
      { src: 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js' },
    ],

  },
  loading: { color: '#744d82' },
  /*
  ** Point to resources
  */
  srcDir: resolve(__dirname, '..', 'resources')
}
