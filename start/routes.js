'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/guides/routing
|
*/

const Route = use('Route')

Route.get('app/initdata', 'UserController.initdata')

Route.post('register', 'UserController.store').middleware('guest')
Route.post('getLogin', 'UserController.userLogin').middleware('guest')
Route.get('app/logout', 'UserController.logout')


Route.any('*', 'NuxtController.render')
