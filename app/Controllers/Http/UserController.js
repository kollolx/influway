'use strict'

const User = use('App/Models/User')
const { validateAll } = use('Validator')

class UserController {

    async initdata({ request, response, auth }) {
        try {
            const user = await auth.getUser()
            if (user) {
                return {
                    user: user,
                }
            }

        } catch (error) {
            console.log(error.message)
            return false
        }
    }

    async store({ request, response, session, auth }) {

        const rules = {
            email: 'required|email|unique:users,email',
            username: 'required|unique:users,username',
            password: 'required',
        }
        const messages = {
            'email.required': 'Email is required',
            'email.unique': 'Email already in use',
            'username.unique': 'Username already in use',
        }

        let data = request.all()

        const validation = await validateAll(data, rules, messages)
        if (validation.fails()) {
            return response.status(401).json(validation.messages())
        }

        if (data.userType == 'influencer') {
            data.role = 2
        }
        if (data.userType == 'retail') {
            data.role = 1
        }

        delete data.confirm_email
        delete data.isAgreed
        delete data.userType

        let user = await User.create(data)

        return user;
    }

    async userLogin({ request, response, auth, session }) {

        const data = request.all()

        try {

            let user = await auth.query().remember(data.rememberMe).attempt(data.email, data.password)
            return user
        } catch (e) {
            console.log(e.message)
            return response.status(401).json(
                {
                    'message': 'Invalid email or password. Please try again.'
                }
            )
        }

    }

    async logout({ auth, session }) {

        session.clear()
        return await auth.logout();
    }
}

module.exports = UserController
