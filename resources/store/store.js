//import Vuex from 'vuex'
export const strict = false
export const state = () => ({
    // all commons states of different pages is stored here
    authUser: false,

})


// common getters
export const getters = {

    isLoggedIn(state) {
        return state.authUser ? true : false
    },

    authUser(state) {
        return state.authUser
    }
}

//mutations for changing data from action

export const mutations = {
    loginUser(state, data) {
        state.authUser = data
    },
    updateAuthUser(state, user) {
        state.authUser = user
    },

    updateCurrentUser(state, data) {
        // update the listing
        state.currentUser = []
        state.currentUser.push(data)

    },

}

// actionns for commiting mutations 

export const actions = {

    async nuxtServerInit({ commit }, { $axios }) {

        try {

            // get the initial data 
            let  data  = await $axios.get('app/initdata')
            console.log('nuxtServerinit')
            console.log(data)
            // update the state of the aiuth
            commit('loginUser', data.user)
            // console.log(data)

        } catch (e) {
            console.log(e.response)
        }
    },

    loginUser({ commit }, data) {
        commit('loginUser', data)
    },

    async logout({ commit, redirect }) {
        try {
            let { data } = await this.$axios.get('app/logout')
            // update the state of the aiuth
            commit('updateAuthUser', false)
            this.$router.push('/login')
        } catch (e) {
            console.log(e)
        }
    }


}



